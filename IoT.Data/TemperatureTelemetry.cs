﻿using System;
using IoT.Business;

namespace IoT.Data
{
    public class TemperatureTelemetry : ILevelTelemetry
    {
        public long Id { get; set; }
        public int HubId { get; set; }
        public Hub Hub { get; set; }
        public DateTime TimeStamp { get; set; }
        public decimal Value { get; set; }
        public string Level { get; set; }
    }
}
