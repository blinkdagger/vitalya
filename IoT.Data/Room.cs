﻿using System.ComponentModel.DataAnnotations;

namespace IoT.Data
{
    public class Room
    {
        public int Id { get; set; }
        [Required]
        public string Number { get; set; }
    }
}