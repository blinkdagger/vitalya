﻿using System;
using System.Collections.Generic;

namespace IoT.Data
{
    public class Hub
    {
        public int Id { get; set; }
        public Guid UniqueId { get; set; }
        public string Name { get; set; }
        public List<Device> Devices { get; set; }
    }
}