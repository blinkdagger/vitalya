﻿using System.ComponentModel.DataAnnotations;

namespace IoT.Data
{
    public class Patient
    {
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public int RoomId { get; set; }
        public Room Room { get; set; }
        public int? HubId { get; set; }
        public Hub Hub { get; set; }
    }
}
