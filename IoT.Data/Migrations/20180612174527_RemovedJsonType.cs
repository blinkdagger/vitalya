﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IoT.Data.Migrations
{
    public partial class RemovedJsonType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "JsonType",
                table: "DeviceTypes");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "JsonType",
                table: "DeviceTypes",
                nullable: false,
                defaultValue: 0);
        }
    }
}
