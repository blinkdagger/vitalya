﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IoT.Data.Migrations
{
    public partial class IndexFotTimestamp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_TemperatureTelemetries_TimeStamp",
                table: "TemperatureTelemetries",
                column: "TimeStamp");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_TemperatureTelemetries_TimeStamp",
                table: "TemperatureTelemetries");
        }
    }
}
