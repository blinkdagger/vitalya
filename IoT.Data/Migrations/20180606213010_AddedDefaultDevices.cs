﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IoT.Data.Migrations
{
    public partial class AddedDefaultDevices : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "DeviceTypes",
                columns: new[] { "Id", "JsonType", "Name" },
                values: new object[] { 1, 0, "Температурный датчик" });

            migrationBuilder.InsertData(
                table: "Devices",
                columns: new[] { "Id", "DeviceTypeId", "HubId" },
                values: new object[] { new Guid("0883f9b5-5093-40c1-ade9-5204e5862e27"), 1, 1 });

            migrationBuilder.InsertData(
                table: "Devices",
                columns: new[] { "Id", "DeviceTypeId", "HubId" },
                values: new object[] { new Guid("190f56aa-ba53-4c68-8566-f333f278fe66"), 1, 2 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Devices",
                keyColumn: "Id",
                keyValue: new Guid("0883f9b5-5093-40c1-ade9-5204e5862e27"));

            migrationBuilder.DeleteData(
                table: "Devices",
                keyColumn: "Id",
                keyValue: new Guid("190f56aa-ba53-4c68-8566-f333f278fe66"));

            migrationBuilder.DeleteData(
                table: "DeviceTypes",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}
