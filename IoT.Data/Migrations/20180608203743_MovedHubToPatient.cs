﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace IoT.Data.Migrations
{
    public partial class MovedHubToPatient : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Hubs_Patients_PatientId",
                table: "Hubs");

            migrationBuilder.DropIndex(
                name: "IX_Hubs_PatientId",
                table: "Hubs");

            migrationBuilder.DropColumn(
                name: "PatientId",
                table: "Hubs");

            migrationBuilder.AddColumn<int>(
                name: "HubId",
                table: "Patients",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "Patients",
                keyColumn: "Id",
                keyValue: 1,
                column: "HubId",
                value: 1);

            migrationBuilder.UpdateData(
                table: "Patients",
                keyColumn: "Id",
                keyValue: 2,
                column: "HubId",
                value: 2);

            migrationBuilder.CreateIndex(
                name: "IX_Patients_HubId",
                table: "Patients",
                column: "HubId");

            migrationBuilder.AddForeignKey(
                name: "FK_Patients_Hubs_HubId",
                table: "Patients",
                column: "HubId",
                principalTable: "Hubs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Patients_Hubs_HubId",
                table: "Patients");

            migrationBuilder.DropIndex(
                name: "IX_Patients_HubId",
                table: "Patients");

            migrationBuilder.DropColumn(
                name: "HubId",
                table: "Patients");

            migrationBuilder.AddColumn<int>(
                name: "PatientId",
                table: "Hubs",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "Hubs",
                keyColumn: "Id",
                keyValue: 1,
                column: "PatientId",
                value: 1);

            migrationBuilder.UpdateData(
                table: "Hubs",
                keyColumn: "Id",
                keyValue: 2,
                column: "PatientId",
                value: 2);

            migrationBuilder.CreateIndex(
                name: "IX_Hubs_PatientId",
                table: "Hubs",
                column: "PatientId");

            migrationBuilder.AddForeignKey(
                name: "FK_Hubs_Patients_PatientId",
                table: "Hubs",
                column: "PatientId",
                principalTable: "Patients",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
