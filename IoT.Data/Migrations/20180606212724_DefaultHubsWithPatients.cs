﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace IoT.Data.Migrations
{
    public partial class DefaultHubsWithPatients : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Patients",
                columns: new[] { "Id", "Name", "RoomId" },
                values: new object[] { 1, "Vasiliy", 1 });

            migrationBuilder.InsertData(
                table: "Patients",
                columns: new[] { "Id", "Name", "RoomId" },
                values: new object[] { 2, "Vitaliy", 1 });

            migrationBuilder.InsertData(
                table: "Hubs",
                columns: new[] { "Id", "PatientId", "UniqueId" },
                values: new object[] { 1, 1, new Guid("c9a01241-0109-4dbb-8502-917b02291aa7") });

            migrationBuilder.InsertData(
                table: "Hubs",
                columns: new[] { "Id", "PatientId", "UniqueId" },
                values: new object[] { 2, 2, new Guid("3678a8c1-53bf-4816-b125-934b05d4c4cf") });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Hubs",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Hubs",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Patients",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Patients",
                keyColumn: "Id",
                keyValue: 2);
        }
    }
}
