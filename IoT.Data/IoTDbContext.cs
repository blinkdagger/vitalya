﻿using System;
using Microsoft.EntityFrameworkCore;

namespace IoT.Data
{
    public class IoTDbContext:DbContext
    {
        public IoTDbContext(DbContextOptions<IoTDbContext> options)
            : base(options)
        { }
        public DbSet<Patient> Patients { get; set; }
        public DbSet<Device> Devices { get; set; }
        public DbSet<DeviceType> DeviceTypes { get; set; }
        public DbSet<TemperatureTelemetry> TemperatureTelemetries { get; set; }
        public DbSet<Hub> Hubs { get; set; }
        public DbSet<Room> Rooms { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TemperatureTelemetry>().HasIndex(s => s.TimeStamp);

            modelBuilder.Entity<Room>().HasData(new Room { Id = 1, Number = "205"});
            
            modelBuilder.Entity<Hub>().HasData(new Hub { Id = 1, UniqueId = Guid.Parse("c9a01241-0109-4dbb-8502-917b02291aa7") });
            modelBuilder.Entity<Hub>().HasData(new Hub { Id = 2, UniqueId = Guid.Parse("3678a8c1-53bf-4816-b125-934b05d4c4cf") });

            modelBuilder.Entity<Patient>().HasData(new Patient { Id = 1, Name = "Vasiliy", RoomId = 1 , HubId = 1});
            modelBuilder.Entity<Patient>().HasData(new Patient { Id = 2, Name = "Vitaliy", RoomId = 1, HubId = 2 });

            modelBuilder.Entity<DeviceType>().HasData(new DeviceType { Id = 1, Name = "Температурный датчик"});

            modelBuilder.Entity<Device>().HasData(new Device { Id = Guid.Parse("0883f9b5-5093-40c1-ade9-5204e5862e27"), DeviceTypeId = 1, HubId = 1});
            modelBuilder.Entity<Device>().HasData(new Device { Id = Guid.Parse("190f56aa-ba53-4c68-8566-f333f278fe66"), DeviceTypeId = 1, HubId = 2 });
        }
    }
    
}
