﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace IoT.Data
{
    public class Device
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }
        [Required]
        public Hub Hub { get; set; }
        public int HubId { get; set; }
        [Required]
        public DeviceType DeviceType { get; set; }
        public int DeviceTypeId { get; set; }

    }
}