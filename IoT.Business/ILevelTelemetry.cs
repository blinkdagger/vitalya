﻿namespace IoT.Business
{
    public interface ILevelTelemetry
    {
        string Level { get; }
    }
}