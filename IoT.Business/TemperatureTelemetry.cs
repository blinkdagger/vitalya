﻿namespace IoT.Business
{
    public class TemperatureTelemetry : DecimalTelemetry, ILevelTelemetry
    {
        public string Level { get; set; }
    }
}