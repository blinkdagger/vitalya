﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IoT.Business
{
    public class TimedValue<T>
    {
        public DateTime TimeStamp { get; set; }
        public T Value { get; set; }
    }
}
