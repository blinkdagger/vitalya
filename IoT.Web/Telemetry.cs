﻿using System;

namespace IoT.Web
{
    public class Telemetry
    {
        public Guid HubId { get; set; }
        public int DeviceTypeId { get; set; }
        public DateTime TimeStamp { get; set; }
        public string Data { get; set; }
    }
}
