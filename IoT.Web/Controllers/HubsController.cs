﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IoT.Data;

namespace IoT.Web.Controllers
{
    public class HubsController : Controller
    {
        private readonly IoTDbContext _context;

        public HubsController(IoTDbContext context)
        {
            _context = context;
        }

        // GET: Hubs
        public async Task<IActionResult> Index()
        {
            return View(await _context.Hubs.ToListAsync());
        }

        // GET: Hubs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hub = await _context.Hubs
                .FirstOrDefaultAsync(m => m.Id == id);
            if (hub == null)
            {
                return NotFound();
            }

            return View(hub);
        }

        // GET: Hubs/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Hubs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,UniqueId")] Hub hub)
        {
            if (ModelState.IsValid)
            {
                _context.Add(hub);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(hub);
        }

        // GET: Hubs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hub = await _context.Hubs.FindAsync(id);
            if (hub == null)
            {
                return NotFound();
            }
            return View(hub);
        }

        // POST: Hubs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,UniqueId")] Hub hub)
        {
            if (id != hub.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(hub);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HubExists(hub.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(hub);
        }

        // GET: Hubs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var hub = await _context.Hubs
                .FirstOrDefaultAsync(m => m.Id == id);
            if (hub == null)
            {
                return NotFound();
            }

            return View(hub);
        }

        // POST: Hubs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var hub = await _context.Hubs.FindAsync(id);
            _context.Hubs.Remove(hub);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HubExists(int id)
        {
            return _context.Hubs.Any(e => e.Id == id);
        }
    }
}
