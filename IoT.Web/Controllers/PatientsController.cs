﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IoT.Data;

namespace IoT.Web.Controllers
{
    public class PatientsController : Controller
    {
        private readonly IoTDbContext _context;
        private static readonly IMapper _mapper;
        static PatientsController()
        {
            var cfg = new MapperConfiguration(opts =>
            {
                opts.CreateMap<Patient, ViewModels.Patient>();
                opts.CreateMap<DeviceType, ViewModels.DeviceType>();
            });
            _mapper = cfg.CreateMapper();
        }
        public PatientsController(IoTDbContext context)
        {
            _context = context;
        }

        // GET: Patients
        public async Task<IActionResult> Index()
        {
            var ioTDbContext = _context.Patients.Include(p => p.Hub).Include(p => p.Room);
            return View(await ioTDbContext.ToListAsync());
        }

        // GET: Patients/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var patient = await _context.Patients
                .Include(p => p.Hub)
                .Include(p => p.Room)
                .FirstOrDefaultAsync(m => m.Id == id);
            
            if (patient == null)
            {
                return NotFound();
            }
            ViewModels.Patient patientVm = _mapper.Map<ViewModels.Patient>(patient);
            if (patient.Hub != null)
            {
                var connectedDevices = _context.Devices.Where(t => t.HubId.Equals(patient.HubId.Value)).Select(d=>d.DeviceType);
                patientVm.DeviceTypes = connectedDevices.Select(d => _mapper.Map<ViewModels.DeviceType>(d)).ToList();
            }
            return View(patientVm);
        }

        // GET: Patients/Create
        public IActionResult Create()
        {
            ViewData["HubId"] = new SelectList(_context.Hubs, "Id", "Id");
            ViewData["RoomId"] = new SelectList(_context.Rooms, "Id", "Number");
            return View();
        }

        // POST: Patients/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Name,RoomId,HubId")] Patient patient)
        {
            if (ModelState.IsValid)
            {
                _context.Add(patient);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["HubId"] = new SelectList(_context.Hubs, "Id", "Id", patient.HubId);
            ViewData["RoomId"] = new SelectList(_context.Rooms, "Id", "Number", patient.RoomId);
            return View(patient);
        }

        // GET: Patients/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var patient = await _context.Patients.FindAsync(id);
            if (patient == null)
            {
                return NotFound();
            }
            ViewData["HubId"] = new SelectList(_context.Hubs, "Id", "Id", patient.HubId);
            ViewData["RoomId"] = new SelectList(_context.Rooms, "Id", "Number", patient.RoomId);
            return View(patient);
        }

        // POST: Patients/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,RoomId,HubId")] Patient patient)
        {
            if (id != patient.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(patient);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PatientExists(patient.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["HubId"] = new SelectList(_context.Hubs, "Id", "Id", patient.HubId);
            ViewData["RoomId"] = new SelectList(_context.Rooms, "Id", "Number", patient.RoomId);
            return View(patient);
        }

        // GET: Patients/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var patient = await _context.Patients
                .Include(p => p.Hub)
                .Include(p => p.Room)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (patient == null)
            {
                return NotFound();
            }

            return View(patient);
        }

        // POST: Patients/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var patient = await _context.Patients.FindAsync(id);
            _context.Patients.Remove(patient);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PatientExists(int id)
        {
            return _context.Patients.Any(e => e.Id == id);
        }
    }
}
