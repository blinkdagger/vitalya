﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using IoT.Business;
using IoT.Data;
using IoT.Web.Hubs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using TemperatureTelemetry = IoT.Business.TemperatureTelemetry;

namespace IoT.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TelemetriesController : ControllerBase
    {
        private static readonly IMapper _mapper;
        private readonly IoTDbContext _context;
        private readonly IHubContext<CriticalTelemetryHub> _hubContext;
        static TelemetriesController()
        {
            var cfg = new MapperConfiguration(opts =>
            {
                opts.CreateMap<Data.TemperatureTelemetry, TimedValue<decimal>>();
            });
            _mapper = cfg.CreateMapper();
        }
        public TelemetriesController(IoTDbContext context, IHubContext<CriticalTelemetryHub> hubContext)
        {
            _context = context;
            _hubContext = hubContext;
        }

        [Route("temperature")]
        [HttpPost]
        public async Task<IActionResult> PostTemperature([FromBody] Telemetry telemetry)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var deserializedTelemetry = JsonConvert.DeserializeObject<TemperatureTelemetry>(telemetry.Data);
            if (deserializedTelemetry.Level.Contains("Critical"))
            {
                await _hubContext.Clients.All.SendAsync("ReceiveMessage", telemetry.HubId, "critical");
            }
            else if (deserializedTelemetry.Level.Contains("Bad"))
            {
                await _hubContext.Clients.All.SendAsync("ReceiveMessage", telemetry.HubId, "bad");
            }
            IoT.Data.TemperatureTelemetry dbTelemetry = new IoT.Data.TemperatureTelemetry()
            {
                Level = deserializedTelemetry.Level,
                Value = deserializedTelemetry.Value,
                Hub = _context.Hubs.First(t => t.UniqueId == telemetry.HubId),
                TimeStamp = telemetry.TimeStamp
            };
            
            _context.TemperatureTelemetries.Add(dbTelemetry);
            await _context.SaveChangesAsync();

            return Ok();
        }
        [Route("temperaturestats")]
        [HttpGet]
        public async Task<IActionResult> GetTemperatureStats(int patientId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var patient = await _context.Patients
                .Include(s => s.Hub)
                .FirstOrDefaultAsync(s => s.Id == patientId);
            if (patient?.Hub == null)
            {
                return BadRequest();
            }
            var resultList = await _context.TemperatureTelemetries
                .Where(s => s.HubId == patient.HubId)
                .OrderByDescending(s => s.TimeStamp)
                .Take(100)
                .ToListAsync();
            resultList.Reverse();
            return Ok(resultList.Select(_mapper.Map<TimedValue<decimal>>));
        }
    }
}