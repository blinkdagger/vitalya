﻿const connection = new signalR.HubConnectionBuilder()
    .withUrl("/criticalhub")
    .build();

connection.on("ReceiveMessage",
    (hubId, level) => {
        var id = "#" + hubId;
        level = level.toLowerCase();
        console.log(id, level);
        if (level === "critical") {
            $(id).css("background-color", "red");
        }
        if (level === "bad") {
            $(id).css("background-color", "yellow");
        }
       
    });

connection.start().catch(err => console.error(err.toString()));