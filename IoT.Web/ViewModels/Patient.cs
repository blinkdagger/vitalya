﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IoT.Web.ViewModels
{
    public class Patient
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int RoomId { get; set; }
        public string RoomNumber { get; set; }
        public int? HubId { get; set; }
        public Guid? HubUniqueId { get; set; }
        public List<DeviceType> DeviceTypes { get; set; }
    }

    public class DeviceType
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
