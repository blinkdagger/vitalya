﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace IoT.Web.Hubs
{
    public class CriticalTelemetryHub : Hub
    {
        public async Task SendMessage(Guid hubId, string level)
        {
            await Clients.All.SendAsync("ReceiveMessage", hubId, level);
        }
    }
}