﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace IoT.Temperature.FakeDevice
{
    internal class Program
    {
        private const string NormalLevel = "Normal";
        private const string BadLevel = "Bad";
        private const string CriticalLevel = "Critical";
        private const double CriticalLowTemperature = 35;
        private const double BadHighTemperature = 37.3;
        private const double CriticalHighTemperature = 38;

        private static readonly HttpClient _httpClient = new HttpClient
        {
            BaseAddress = new Uri("http://localhost:8629")
        };

        private static void Main(string[] args)
        {
            SendDeviceToCloudMessagesAsync();
            Console.ReadLine();
        }

        private static async void SendDeviceToCloudMessagesAsync()
        {
            double minTemperature = 36.2;
            double maxTemperature = 42;
            Random rand = new Random();
            double currentTemperature = minTemperature;
            while (true)
            {
                var randInt = rand.Next(-1, 4);
                currentTemperature = currentTemperature + rand.NextDouble() * 0.05 * randInt;
                if (currentTemperature < minTemperature)
                {
                    currentTemperature += 0.5;
                }
                if (currentTemperature > maxTemperature)
                {
                    currentTemperature -= 0.5;
                }
                string MessageLevel = NormalLevel;
                if (currentTemperature < CriticalHighTemperature && currentTemperature > BadHighTemperature)
                {
                    MessageLevel = BadLevel;
                }
                else if (currentTemperature > CriticalHighTemperature)
                {
                    MessageLevel = CriticalLevel;
                }
                var telemetry = new
                {
                    Value = currentTemperature,
                    Level = MessageLevel
                };
                var telemetryDataPoint = new
                {
                    HubId = "3678a8c1-53bf-4816-b125-934b05d4c4cf",
                    DeviceTypeId = 1,
                    TimeStamp = DateTime.UtcNow,
                    Data = JsonConvert.SerializeObject(telemetry)
                };
                var telemetryDataString = JsonConvert.SerializeObject(telemetryDataPoint);
                try
                {
                    await _httpClient.PostAsync("api/telemetries/temperature",
                        new StringContent(telemetryDataString, Encoding.UTF8, "application/json"));
                    //set the body of the message to the serialized value of the telemetry data
                    Console.WriteLine("{0} > Sent message: {1}", DateTime.Now, telemetryDataString);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Send failed");
                }

                await Task.Delay(5000);
            }
        }
    }
}